﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleAssignment
{
    //class that analyzes the sides inputed by the user and response what kind of triangle is
    public static class TriangleSolver
    {
        public static string Analyze(int side1, int side2, int side3)
        {
            string message = "";
            //if one of the sides are less than 0, it is not a valid triangle
            if(side1 > 0 && side2 > 0 && side3 > 0)
            {
                if(side1 == side2 && side2 == side3)
                {
                    message = "Perfect! You have an equilateral triangle.";
                }
                else if(side1 == side2 || side2 == side3 || side1 == side3)
                {
                    message = "Perfect! You have an isosceles triangle.";
                }
                else
                {
                    message = "Perfect! You have an scalene triangle.";
                }
            }
            else
            {
                message = "To be a valid triangle, please insert only numbers greater than 0.";
            }

            return message;
        }
    }
}
