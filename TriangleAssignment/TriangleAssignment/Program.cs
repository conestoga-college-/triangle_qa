﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Menu();
        }

        //presents the menu and expects the input from the user
        public void Menu()
        {
            string sOption = string.Empty;
            int option = 0;

            //shows the menu until the user inputs a right option
            do
            {
                Console.WriteLine("1. Enter triangle dimensions");
                Console.WriteLine("2. Exit");
                Console.WriteLine("");
                sOption = Console.ReadLine();
            } while (!int.TryParse(sOption, out option) || option < 1 || option > 2);

            //depends on the input, an operation is made
            switch (option)
            {
                case 1:
                    Console.WriteLine(ReceiveInputs());
                    Console.WriteLine("");
                    break;
                case 2:
                    Environment.Exit(0);
                    break;
            }

            Menu();
        }

        //funtion to receive and analize inputs from user
        public string ReceiveInputs()
        {
            string sSide1 = string.Empty;
            string sSide2 = string.Empty;
            string sSide3 = string.Empty;
            int side1 = 0;
            int side2 = 0;
            int side3 = 0;

            Console.WriteLine("Write the size of the first side of your triangle: ");
            sSide1 = Console.ReadLine();

            //only receive numbers. If the user tries to input letters, will ask again for a correct input
            while (!int.TryParse(sSide1, out side1))
            {
                Console.WriteLine("The triangle side must be a number.");
                Console.WriteLine("Write the size of the first side of your triangle: ");
                sSide1 = Console.ReadLine();
            }

            Console.WriteLine("");
            Console.WriteLine("Write the size of the second side of your triangle: ");
            sSide2 = Console.ReadLine();

            //only receive numbers. If the user tries to input letters, will ask again for a correct input
            while (!int.TryParse(sSide2, out side2))
            {
                Console.WriteLine("The triangle side must be a number.");
                Console.WriteLine("Write the size of the second side of your triangle: ");
                sSide2 = Console.ReadLine();
            }

            Console.WriteLine("");
            Console.WriteLine("Write the size of the third side of your triangle: ");
            sSide3 = Console.ReadLine();

            //only receive numbers. If the user tries to input letters, will ask again for a correct input
            while (!int.TryParse(sSide3, out side3))
            {
                Console.WriteLine("The triangle side must be a number.");
                Console.WriteLine("Write the size of the third side of your triangle: ");
                sSide3 = Console.ReadLine();
            }
            Console.WriteLine("");

            //checking a valid triangle using TriangleSolver.Analyse method and returning the result
            return TriangleSolver.Analyze(side1, side2, side3);
        }
    }
}
