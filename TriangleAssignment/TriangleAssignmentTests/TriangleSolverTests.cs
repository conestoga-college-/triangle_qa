﻿using TriangleAssignment;
using NUnit.Framework;

namespace TriangleAssignment.Tests
{
    [TestFixture]
    public class TriangleSolverTests
    {
        //initializing global variables
        int side1 = 0;
        int side2 = 0;
        int side3 = 0;

        //initializing possible response messages
        string equilateral = "Perfect! You have an equilateral triangle.";
        string isosceles = "Perfect! You have an isosceles triangle.";
        string scalene = "Perfect! You have an scalene triangle.";
        string notValid = "To be a valid triangle, please insert only numbers greater than 0.";

        // Test Identifier: 1.1
        // Test Description: Trying to build a triangle with negative values
        // Input Data: negative number
        // Expected Output: not a valid triangle
        [Test]
        public void AnalyzeTest_negativeValues_notValidTriangle()
        {
            // 1. Arrange
            side1 = -2;
            side2 = 9;
            side3 = 7;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(notValid, response);
        }

        // Test Identifier: 1.2
        // Test Description: Building an equilateral triangle
        // Input Data: all sides have the same size
        // Expected Output: equilateral triangle
        [Test]
        public void AnalyzeTest_sameSidesValues_equilateral()
        {
            // 1. Arrange
            side1 = 8;
            side2 = 8;
            side3 = 8;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(equilateral, response);
        }

        // Test Identifier: 1.3
        // Test Description: Building an isosceles triangle
        // Input Data: only 2 sides have the same size
        // Expected Output: isosceles triangle
        [Test]
        public void AnalyzeTest_twoSameSidesValues_isosceles()
        {
            // 1. Arrange
            side1 = 5;
            side2 = 8;
            side3 = 5;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(isosceles, response);
        }

        // Test Identifier: 1.4
        // Test Description: Building a scalene triangle
        // Input Data: all the sides have different sizes
        // Expected Output: scalene triangle
        [Test]
        public void AnalyzeTest_differentSidesValues_scalene()
        {
            // 1. Arrange
            side1 = 2;
            side2 = 3;
            side3 = 4;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(scalene, response);
        }

        // Test Identifier: 1.5
        // Test Description: Trying to build a triangle with zero values
        // Input Data: all sidez with zero
        // Expected Output: not a valid triangle
        [Test]
        public void AnalyzeTest_zeroValues_notValidTriangle()
        {
            // 1. Arrange
            side1 = 0;
            side2 = 0;
            side3 = 0;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(notValid, response);
        }

        // Test Identifier: 1.6
        // Test Description: Trying to build a triangle with only one zero value
        // Input Data: all sides with zero
        // Expected Output: not a valid triangle
        [Test]
        public void AnalyzeTest_oneZeroValues_notValidTriangle()
        {
            // 1. Arrange
            side1 = 0;
            side2 = 4;
            side3 = 5;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(notValid, response);
        }

        // Test Identifier: 1.7
        // Test Description: Trying to build a triangle with zwero and negative values
        // Input Data: one side with zero value and one side with negative value
        // Expected Output: not a valid triangle
        [Test]
        public void AnalyzeTest_zeroAndNegativeValues_notValidTriangle()
        {
            // 1. Arrange
            side1 = 0;
            side2 = -4;
            side3 = 5;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(notValid, response);
        }

        // Test Identifier: 1.8
        // Test Description: Trying to build a triangle with all negative values
        // Input Data: all negative values
        // Expected Output: not a valid triangle
        [Test]
        public void AnalyzeTest_allNegativeValues_notValidTriangle()
        {
            // 1. Arrange
            side1 = -1;
            side2 = -4;
            side3 = -1;

            // 2. Act
            string response = TriangleSolver.Analyze(side1, side2, side3);

            // 3. Assert
            Assert.AreEqual(notValid, response);
        }
    }
}